import 'react-native-gesture-handler';
import React from 'react';
import { LogBox, StatusBar } from 'react-native';

import Routes from './routes/index.routes';

const App: React.FC = () => {
  LogBox.ignoreLogs(['Expected style']);

  return (
    <>
      <StatusBar
        barStyle="light-content"
        translucent
        backgroundColor="transparent"
      />
      <Routes />
    </>
  );
};

export default App;
