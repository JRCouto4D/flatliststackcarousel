import styled from 'styled-components/native';
import { responsiveWidth as ww } from 'react-native-responsive-dimensions';
import Animated from 'react-native-reanimated';

import TextBase from '../TextDynamic';

export const Container = styled.View`
  width: ${ww(100)};

  position: absolute;
  bottom: 5%;

  align-items: center;
  justify-content: center;
`;

export const Text = styled(TextBase)``;

export const ToggleThemeButton = styled.TouchableOpacity`
  width: 120px;
  height: 50px;
  border-radius: 25px;

  margin-top: 10px;
  padding: 0 3px;

  border-width: 3px;
  border-color: #fff;

  justify-content: center;
`;

export const Indicator = styled(Animated.View)`
  width: 40px;
  height: 40px;
  border-radius: 20px;
`;
