import React from 'react';
import {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import { darkTheme, lightTheme } from '../../services/theme';

import { Container, Text, ToggleThemeButton, Indicator } from './styles';

interface ToggleThemeProps {
  handleTheme(): void;
  light: boolean;
}

const ToggleTheme: React.FC<ToggleThemeProps> = ({ handleTheme, light }) => {
  const theme = light ? lightTheme : darkTheme;

  const indicatorPosition = useSharedValue(0);

  const indicatorStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: indicatorPosition.value }],
  }));

  return (
    <Container>
      <Text isColor={theme.textColor} isSize={2} style={{ fontWeight: 'bold' }}>
        Dark Theme
      </Text>

      <ToggleThemeButton
        activeOpacity={1}
        onPress={() => {
          if (!light) {
            indicatorPosition.value = withTiming(0, { duration: 500 });
          } else {
            indicatorPosition.value = withTiming(68, { duration: 500 });
          }

          handleTheme();
        }}
        style={{ backgroundColor: theme.indicatorBackground }}>
        <Indicator
          style={[indicatorStyle, { backgroundColor: theme.indicatorColor }]}
        />
      </ToggleThemeButton>
    </Container>
  );
};

export default ToggleTheme;
