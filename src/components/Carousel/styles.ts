import styled from 'styled-components/native';
import { Animated } from 'react-native';
import {
  responsiveHeight as wh,
  responsiveWidth as ww,
} from 'react-native-responsive-dimensions';
import LinearGradient from 'react-native-linear-gradient';

import { MoviesType } from '../../services/movies';

export const Container = styled.View`
  width: ${ww(100)};
  height: ${wh(100)};
`;

export const MoviesList = styled(
  Animated.FlatList as new () => Animated.FlatList<MoviesType>,
).attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})``;

export const BackdropContainer = styled.View`
  width: ${ww(100)};
  height: ${wh(60)};

  position: absolute;
`;

export const Content = styled(Animated.View)``;

export const BackdropImage = styled.Image`
  width: ${ww(100)};
  height: ${wh(60)};
`;

export const Gradient = styled(LinearGradient)`
  width: ${ww(100)};
  height: ${wh(60)};

  position: absolute;
  bottom: 0;
`;
