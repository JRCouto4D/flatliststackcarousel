import styled from 'styled-components/native';

import TextBase from '../../../TextDynamic';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;

  margin-top: 2%;
`;

export const Content = styled.View`
  border-width: 0.5px;
  border-color: #999;
  border-radius: 20px;

  padding: 1.5% 7%;
`;

export const Text = styled(TextBase)``;
