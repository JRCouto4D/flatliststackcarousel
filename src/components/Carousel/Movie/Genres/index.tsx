import React from 'react';

import { Container, Content, Text } from './styles';

interface GenresProps {
  data: Array<string>;
  theme: {
    backgroundColor: string;
    textColor: string;
  };
}
const Genres: React.FC<GenresProps> = ({ data, theme }) => {
  return (
    <Container>
      {data.map((item, index) => (
        <Content
          key={String(index)}
          style={{ marginHorizontal: 2, marginVertical: '2%' }}>
          <Text isColor={theme.textColor}>{item}</Text>
        </Content>
      ))}
    </Container>
  );
};

export default Genres;
