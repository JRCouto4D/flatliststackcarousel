import styled from 'styled-components/native';
import { Animated } from 'react-native';
import { ITEM_SIZE, SPACING } from '../index';

import TextBase from '../../TextDynamic';

export const Container = styled(Animated.View)`
  width: ${ITEM_SIZE};
`;

export const Content = styled.View`
  padding: 20px;
  align-items: center;
  background: #312e38;
  border-radius: 34px;
`;

export const PostImage = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: 100%;
  height: ${ITEM_SIZE * 1.2};
  border-radius: 24px;
  margin: 0;
  margin-bottom: 10px;
`;

export const Text = styled(TextBase)``;
