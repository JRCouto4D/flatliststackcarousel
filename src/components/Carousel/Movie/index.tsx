import React from 'react';

import { MoviesType } from '../../../services/movies';

import Rating from './Rating';
import Genres from './Genres';
import { Container, Content, PostImage, Text } from './styles';

interface MovieProps {
  data: MoviesType;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  translateY: any;
  theme: {
    backgroundColor: string;
    textColor: string;
  };
}

const Movie: React.FC<MovieProps> = ({ data, translateY, theme }) => {
  return (
    <Container style={{ transform: [{ translateY }] }}>
      <Content
        style={{
          marginHorizontal: 10,
          backgroundColor: theme.backgroundColor,
        }}>
        <PostImage source={{ uri: data.poster }} />

        <Text
          isColor={theme.textColor}
          isSize={2}
          numberOfLines={1}
          style={{ fontWeight: '700' }}>
          {data.title}
        </Text>

        <Rating rating={data.rating} theme={theme} />
        <Genres data={data.genres} theme={theme} />

        <Text
          numberOfLines={4}
          isColor={theme.textColor}
          style={{ marginVertical: '5%', textAlign: 'justify' }}>
          {data.description}
        </Text>
      </Content>
    </Container>
  );
};

export default Movie;
