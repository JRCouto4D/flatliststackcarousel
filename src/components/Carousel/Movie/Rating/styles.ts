import styled from 'styled-components/native';
import TextBase from '../../../TextDynamic';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const Text = styled(TextBase)``;
