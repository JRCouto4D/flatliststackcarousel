import React from 'react';
import Material from 'react-native-vector-icons/MaterialIcons';

import { Container, Text } from './styles';

interface RatingProps {
  rating: number;
  theme: {
    backgroundColor: string;
    textColor: string;
  };
}
const Rating: React.FC<RatingProps> = ({ rating, theme }) => {
  const filledStars = Math.floor(rating / 2);
  const maxStars = Array(5 - filledStars).fill({
    name: 'star-border',
    color: '#999',
  });
  const current = [
    ...Array(filledStars).fill({ name: 'star', color: 'tomato' }),
    ...maxStars,
  ];

  return (
    <Container>
      <Text
        isColor={theme.textColor}
        isSize={1.3}
        style={{ fontFamily: 'Menlo', marginRight: '2%' }}>
        {rating}
      </Text>
      {current.map((item, index) => (
        <Material
          key={String(index)}
          name={item.name}
          size={12}
          color={item.color}
        />
      ))}
    </Container>
  );
};

export default Rating;
