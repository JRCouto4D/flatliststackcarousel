import React, { useRef, useCallback } from 'react';
import { Platform, Animated, View } from 'react-native';
import {
  responsiveWidth as ww,
  responsiveHeight as wh,
} from 'react-native-responsive-dimensions';

import movies from '../../services/movies';

import Movie from './Movie';

import {
  Container,
  MoviesList,
  BackdropContainer,
  Content,
  BackdropImage,
  Gradient,
} from './styles';

interface CarouselProps {
  theme: {
    backgroundColor: string;
    textColor: string;
  };
  light: boolean;
}

const WIDTH = ww(100);
const HEIGHT = wh(100);
export const ITEM_SIZE = Platform.OS === 'ios' ? ww(56) : ww(58);
export const SPACING = 10;
export const SPACER_ITEM_SIZE = (WIDTH - ITEM_SIZE) / 2;

const Carousel: React.FC<CarouselProps> = ({ theme }) => {
  const scrollX = useRef(new Animated.Value(0)).current;

  const renderBackdrop = useCallback(() => {
    return (
      <BackdropContainer>
        {movies.map((item, index) => {
          const inputRange = [(index - 1) * ITEM_SIZE, index * ITEM_SIZE];

          const translateX = scrollX.interpolate({
            inputRange,
            outputRange: [WIDTH, 0],
          });

          return (
            <Content
              key={item.key}
              style={{
                width: WIDTH,
                height: HEIGHT,
                transform: [{ translateX }],
                position: 'absolute',
              }}>
              <View>
                <BackdropImage
                  source={{ uri: item.backdrop }}
                  resizeMode="cover"
                />

                <Gradient colors={['transparent', theme.backgroundColor]} />
              </View>
            </Content>
          );
        })}
      </BackdropContainer>
    );
  }, [scrollX, theme]);

  return (
    <Container>
      {renderBackdrop()}

      <MoviesList
        data={movies}
        keyExtractor={item => item.key}
        contentContainerStyle={{ alignItems: 'center' }}
        snapToInterval={ITEM_SIZE}
        decelerationRate={0}
        bounces={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: true },
        )}
        scrollEventThrottle={16}
        ListHeaderComponent={<View style={{ width: SPACER_ITEM_SIZE }} />}
        ListFooterComponent={<View style={{ width: SPACER_ITEM_SIZE }} />}
        renderItem={({ item, index }) => {
          // if (!item.poster) {
          //   return <View style={{ width: SPACER_ITEM_SIZE }} />;
          // }

          const inputRange = [
            (index - 1) * ITEM_SIZE,
            index * ITEM_SIZE,
            (index + 1) * ITEM_SIZE,
          ];

          const translateY = scrollX.interpolate({
            inputRange,
            outputRange: [100, 50, 100],
          });

          // console.log({ translateY });

          return <Movie data={item} translateY={translateY} theme={theme} />;
        }}
      />
    </Container>
  );
};

export default Carousel;
