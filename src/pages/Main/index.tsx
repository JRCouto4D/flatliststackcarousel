import React, { useState } from 'react';

import { lightTheme, darkTheme } from '../../services/theme';

import ToggleTheme from '../../components/ToggleTheme';

import Carousel from '../../components/Carousel';
import { Container } from './styles';

const Main: React.FC = () => {
  const [light, setLight] = useState(true);

  const theme = light ? lightTheme : darkTheme;

  return (
    <Container style={{ backgroundColor: theme.backgroundColor }}>
      <Carousel light={light} theme={theme} />

      <ToggleTheme light={light} handleTheme={() => setLight(!light)} />
    </Container>
  );
};

export default Main;
