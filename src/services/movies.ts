export interface MoviesType {
  key: string;
  title: string;
  poster: string;
  backdrop: string;
  rating: number;
  description: string;
  releaseDate: string;
  genres: Array<string>;
}

type MoviesProps = MoviesType[];

const movies: MoviesProps = [
  {
    key: '1',
    title: 'Corriga',
    description:
      'Isolado, intimidado e desconsiderado pela sociedade, o fracassado comediante Arthur Fleck inicia seu caminho como uma mente criminosa após assassinar três homens em pleno metrô. Sua ação inicia um movimento popular contra a elite de Gotham City, da qual Thomas Wayne é seu maior representante.',
    poster:
      'https://img.elo7.com.br/product/zoom/2A1A4B7/big-poster-filme-joker-coringa-joaquin-phoenix-tam-90x60-cm-nerd.jpg',
    backdrop:
      'https://img.elo7.com.br/product/original/2A1A4BF/big-poster-filme-joker-coringa-joaquin-phoenix-lo02-90x60-cm-poster.jpg',
    rating: 9.2,
    genres: ['Crime', 'Drama'],
    releaseDate: '2019-10-03T00:00:00-03:00',
  },
  {
    key: '2',
    title: 'Senhor dos anéis - A sociedade do anel',
    description:
      'Em uma terra fantástica e única, um hobbit recebe de presente de seu tio um anel mágico e maligno que precisa ser destruído antes que caia nas mãos do mal. Para isso, o hobbit Frodo tem um caminho árduo pela frente, onde encontra perigo, medo e seres bizarros. Ao seu lado para o cumprimento desta jornada, ele aos poucos pode contar com outros hobbits, um elfo, um anão, dois humanos e um mago, totalizando nove seres que formam a Sociedade do Anel.',
    poster:
      'https://img.elo7.com.br/product/zoom/2692717/poster-o-senhor-dos-aneis-a-sociedade-do-anel-lo02-90x60-cm-presente-geek.jpg',
    backdrop:
      'https://img.elo7.com.br/product/zoom/26926E8/poster-o-senhor-dos-aneis-a-sociedade-do-anel-lo01-90x60-cm-o-senhor-dos-aneis.jpg',
    rating: 9.2,
    genres: ['Fantasia', 'Aventura'],
    releaseDate: '2002-12-27T00:00:00-03:00',
  },
  {
    key: '3',
    title: 'O senhor dos anéis - As duas torres',
    description:
      'Após a captura de Merry e Pippy pelos orcs, a Sociedade do Anel é dissolvida. Frodo e Sam seguem sua jornada rumo à Montanha da Perdição para destruir o anel e descobrem que estão sendo perseguidos pelo misterioso Gollum. Enquanto isso, Aragorn, o elfo e arqueiro Legolas e o anão Gimli partem para resgatar os hobbits sequestrados e chegam ao reino de Rohan, onde o rei Theoden foi vítima de uma maldição mortal de Saruman.',
    poster:
      'https://img.elo7.com.br/product/zoom/26927C9/poster-o-senhor-dos-aneis-as-duas-torres-lo01-tam-90x60-cm-presente-geek.jpg',
    backdrop:
      'https://img.elo7.com.br/product/zoom/26927E8/poster-o-senhor-dos-aneis-as-duas-torres-lo03-tam-90x60-cm-nerd.jpg',
    rating: 9.5,
    genres: ['Fantasia', 'Aventura'],
    releaseDate: '2002-01-01T00:00:00-03:00',
  },
  {
    key: '4',
    title: 'Senhor dos anéis - O retorno do rei',
    description:
      'O confronto final entre as forças do bem e do mal que lutam pelo controle do futuro da Terra Média se aproxima. Sauron planeja um grande ataque a Minas Tirith, capital de Gondor, o que faz com que Gandalf e Pippin partam para o local na intenção de ajudar a resistência. Um exército é reunido por Theoden em Rohan, em mais uma tentativa de deter as forças de Sauron. Enquanto isso, Frodo, Sam e Gollum seguem sua viagem rumo à Montanha da Perdição para destruir o anel.',
    poster:
      'https://img.elo7.com.br/product/zoom/2692949/big-poster-o-senhor-dos-aneis-o-retorno-do-rei-lo09-90x60-cm-o-senhor-dos-aneis-o-retorno-do-rei.jpg',
    backdrop:
      'https://img.elo7.com.br/product/zoom/2692938/big-poster-o-senhor-dos-aneis-o-retorno-do-rei-lo08-90x60-cm-presente-geek.jpg',
    genres: ['Fantasia', 'Aventura'],
    rating: 9.3,
    releaseDate: '2003-12-25T00:00:00-03:00',
  },
  {
    key: '5',
    title: 'O Hobbit: uma jornada inesperada',
    description:
      'Como a maioria dos hobbits, Bilbo Bolseiro leva uma vida tranquila até o dia em que recebe uma missão do mago Gandalf. Acompanhado por um grupo de anões, ele parte numa jornada até a Montanha Solitária para libertar o Reino de Erebor do dragão Smaug.',
    poster:
      'https://img.elo7.com.br/product/zoom/268E0A6/big-poster-o-hobbit-uma-jornada-inesperada-lo06-tam-90x60-cm-nerd.jpg',
    backdrop:
      'https://img.elo7.com.br/product/zoom/268DE63/big-poster-o-hobbit-uma-jornada-inesperada-lo01-tam-90x60-cm-presente-geek.jpg',
    rating: 8.3,
    genres: ['Fantasia', 'Aventura'],
    releaseDate: '2012-12-14T00:00:00-03:00',
  },
  {
    key: '6',
    title: 'O Hobbit: a desolação de Smaug',
    description:
      'Ao lado de um grupo de anões e de Gandalf, Bilbo segue em direção à Montanha Solitária, onde deverá ajudar seus companheiros a retomar a Pedra de Arken. O problema é que o artefato está perdido em meio a um tesouro protegido pelo temido dragão Smaug.',
    poster:
      'https://img.elo7.com.br/product/zoom/2692268/big-poster-o-hobbit-a-desolacao-de-smaug-lo07-tam-90x60-cm-o-hobbit.jpg',
    backdrop:
      'https://img.elo7.com.br/product/zoom/268E2FD/big-poster-o-hobbit-a-desolacao-de-smaug-lo01-tam-90x60-cm-poster-cinema.jpg',
    rating: 8.5,
    genres: ['Fantasia', 'Aventura'],
    releaseDate: '2013-12-13T00:00:00-03:00',
  },
  {
    key: '7',
    title: 'O Hobbit: A batalha dos cinco exercitos',
    description:
      'O dragão Smaug lança sua fúria ardente contra a Cidade do Lago que fica próxima da montanha de Erebor. Bard consegue derrotá-lo, mas, rapidamente, sem a ameaça do dragão, inicia-se uma batalha pelo controle de Erebor e sua riqueza. Os anões, liderados por Thorin, adentram a montanha e estão dispostos a impedir a entrada de elfos, anões e orcs. Bilbo Bolseiro e Gandalf tentam impedir a guerra.',
    poster:
      'https://img.elo7.com.br/product/zoom/269238C/poster-o-hobbit-3-a-batalha-dos-cinco-exercitos-lo07-90x60-geek.jpg',
    backdrop:
      'https://img.elo7.com.br/product/zoom/2692347/poster-o-hobbit-3-a-batalha-dos-cinco-exercitos-lo03-90x60-poster-cinema.jpg',
    rating: 8.5,
    genres: ['Fantasia', 'Aventura'],
    releaseDate: '2014-12-07T00:00:00-03:00',
  },
];

export default movies;
