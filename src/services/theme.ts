export const darkTheme = {
  backgroundColor: '#312e38',
  textColor: '#fff',
  indicatorBackground: '#66cdaa',
  indicatorColor: '#7fffd4',
};

export const lightTheme = {
  backgroundColor: '#e5e5e5',
  textColor: '#312e38',
  indicatorBackground: '#C0C0C0',
  indicatorColor: '#A9A9A9',
};
