import React from 'react';

import { NotificationProvider } from './notifications';

const AppProvider: React.FC = ({ children }) => (
  <NotificationProvider>{children}</NotificationProvider>
);

export default AppProvider;
