<span align="center">
  Trata-se de uma aplicação mobile densevolvida com React Native.
</span>

<h1 align="center" display="flex" flexDirection="column">
  <img alt="screnn" title="screen" src="./assets/screen.gif" width="120px" height="360px" />
</h1>


## Ambiente 

Este procedimento foi testado usando o Windows 10 e o emulador no Android Studio.

## Mobile

Faça o download do repositório e no terminal rode o seguinte comando para instalar as dependências:

```
  yarn ou npm install
```

Agora basta iniciar a aplicação 

```
yarn android ou react-native run-android
```





